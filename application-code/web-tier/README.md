# Setup Frontend (ReactJS)

Connect to WebTier EC2 Instance from the AWS Console. Instance > Connect > Session Manager

Switch to ec2-user
```
sudo -su ec2-user
cd ~
```

Update nginx configuration
```
cd /etc/nginx
ls
sudo vim nginx.conf
```

Copy internal_lb_dns_name from terraform output and replace in the below section of the .conf file
```
 #proxy for internal lb
        location /api/{
                proxy_pass http://[REPLACE-WITH-INTERNAL-LB-DNS]:80/;
        }
```
